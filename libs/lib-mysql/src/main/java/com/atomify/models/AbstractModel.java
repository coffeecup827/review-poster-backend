package com.atomify.models;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Version;

@MappedSuperclass
public abstract class AbstractModel {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(updatable = false)
  protected long id;

  @Column(updatable = false)
  private long createdAt;

  @Column
  private long updatedAt;

  @Version
  private long rowVersion;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(long createdAt) {
    this.createdAt = createdAt;
  }

  public long getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(long updatedAt) {
    this.updatedAt = updatedAt;
  }

  public long getRowVersion() {
    return rowVersion;
  }

  public void setRowVersion(long rowVersion) {
    this.rowVersion = rowVersion;
  }


  @PrePersist
  void prePersist() {
    if (this.getCreatedAt() <= 0l) {
      this.setCreatedAt(System.currentTimeMillis());
    }
    this.setUpdatedAt(this.getCreatedAt());
  }

  @PreUpdate
  void preUpdate() {
    this.setUpdatedAt(System.currentTimeMillis());
  }

}
