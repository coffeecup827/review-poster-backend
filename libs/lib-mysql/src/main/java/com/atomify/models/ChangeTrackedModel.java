package com.atomify.models;

import com.atomify.configs.Context;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

@MappedSuperclass
public abstract class ChangeTrackedModel extends AbstractModel {

  @Column
  private long createdBy;

  @Column
  private long updatedBy;

  public long getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(long createdBy) {
    this.createdBy = createdBy;
  }

  public long getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(long updatedBy) {
    this.updatedBy = updatedBy;
  }


  @PrePersist
  void prePersist() {
    super.prePersist();
    if (this.getCreatedBy() <= 0L) {
      this.setCreatedBy(Context.getContext().getUserId());
    }
    this.setUpdatedBy(this.getCreatedBy());
  }

  @PreUpdate
  void preUpdate() {
    super.preUpdate();
    this.setUpdatedBy(Context.getContext().getUserId());
  }

}
