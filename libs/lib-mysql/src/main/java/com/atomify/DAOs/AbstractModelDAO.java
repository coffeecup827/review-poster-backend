package com.atomify.DAOs;

import com.atomify.BaseListRequest;
import com.atomify.models.AbstractModel;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.List;

public abstract class AbstractModelDAO<T extends AbstractModel> {

  @Autowired
  private EntityManager em;

  private final Class<T> modelType;

  public AbstractModelDAO() {
    this.modelType = getModelType();
  }

  protected EntityManager getEm() {
    return em;
  }

  @SuppressWarnings("unchecked")
  protected Class<T> getModelType() {
    if (modelType == null) {
      return (Class<T>)((ParameterizedType)getClass().getGenericSuperclass())
          .getActualTypeArguments()[0];
    } else {
      return modelType;
    }
  }

  public T getNewInstance() {
    Class<T> clazz = getModelType();
    try {
      T t = clazz.getDeclaredConstructor().newInstance();
      return t;
    } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
      throw new RuntimeException("");
    }
  }

  public T create(T instance)  {
    getEm().persist(instance);
    return instance;
  }

  public void delete(T persistentInstance) {
    getEm().remove(persistentInstance);
  }

  public <U extends BaseListRequest> List<T> find(U listRequest) {
    CriteriaBuilder criteriaBuilder = getEm().getCriteriaBuilder();
    CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(this.modelType);
    Root<T> root = criteriaQuery.from(this.modelType);

    if (listRequest != null) {

      if (listRequest.getFilterByIds() != null && !listRequest.getFilterByIds().isEmpty()) {
        criteriaQuery.where(
            criteriaBuilder.in(root.get("id")).value(listRequest.getFilterByIds())
        );
      }

      buildQuery(criteriaQuery, criteriaBuilder, root, listRequest);

      TypedQuery<T> query = getEm().createQuery(criteriaQuery);
      int pageNumber = listRequest.getPageNumber() > 0 ? listRequest.getPageNumber() : 1;
      int pageSize = listRequest.getPageSize() > 0 ? listRequest.getPageSize() : 10;
      query.setFirstResult((pageNumber - 1) * pageSize);
      query.setMaxResults(pageSize);
      return query.getResultList();
    } else {
      TypedQuery<T> query = getEm().createQuery(criteriaQuery);
      query.setFirstResult(0);
      query.setMaxResults(10);
      return query.getResultList();
    }
  }

  public <U extends BaseListRequest> CriteriaQuery<T> buildQuery(CriteriaQuery<T> criteriaQuery, CriteriaBuilder criteriaBuilder, Root<T> root, U listRequest) {
    return criteriaQuery;
  }

  public <U extends BaseListRequest> T findOne(U listRequest) {
    List<T> list = find(listRequest);
    return list == null || list.isEmpty() ? null : list.get(0);
  }

}
