package com.atomify;

import com.atomify.requests.BaseRequest;

import java.util.Set;

public class BaseListRequest extends BaseRequest {

  private Set<Long> filterByIds;
  private int pageNumber;
  private int pageSize;

  public Set<Long> getFilterByIds() {
    return filterByIds;
  }

  public void setFilterByIds(Set<Long> filterByIds) {
    this.filterByIds = filterByIds;
  }

  public int getPageNumber() {
    return pageNumber;
  }

  public void setPageNumber(int pageNumber) {
    this.pageNumber = pageNumber;
  }

  public int getPageSize() {
    return pageSize;
  }

  public void setPageSize(int pageSize) {
    this.pageSize = pageSize;
  }
}
