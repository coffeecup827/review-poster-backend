package com.atomify.configs;

import org.springframework.util.StringUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class AbstractAppInterceptor extends HandlerInterceptorAdapter {

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

    if (StringUtils.isEmpty("user_id")) {
      return false;
    }

    DefaultContext context = new DefaultContext(Long.parseLong(request.getHeader("user_id")));
    Context.setContext(context);

    return super.preHandle(request, response, handler);
  }

}
