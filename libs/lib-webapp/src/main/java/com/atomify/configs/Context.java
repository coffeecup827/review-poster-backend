package com.atomify.configs;

public abstract class Context {
  private static final ThreadLocal<DefaultContext> tContext = new InheritableThreadLocal<DefaultContext>();

  static final DefaultContext setContext(DefaultContext context) {
    tContext.set(context);
    return context;
  }

  public static final DefaultContext getContext() {
    return tContext.get();
  }

}
