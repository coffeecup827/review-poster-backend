package com.atomify.configs;

public class DefaultContext {
  private long userId;

  public long getUserId() {
    return userId;
  }

  public DefaultContext(long userId) {
    this.userId = userId;
  }

}
