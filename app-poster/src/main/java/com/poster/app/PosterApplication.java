package com.poster.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PosterApplication {
  private static final Logger logger = LoggerFactory.getLogger(PosterApplication.class);
  public static void main(String[] args) {
    SpringApplication.run(PosterApplication.class, args);
  }
}
