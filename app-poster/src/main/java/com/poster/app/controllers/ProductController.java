package com.poster.app.controllers;

import com.atomify.BaseListRequest;
import com.atomify.configs.Context;
import com.poster.app.services.FunctionalityService;
import com.poster.models.Product;
import com.poster.models.ProductReview;
import com.poster.models.ProductReviewAggregate;
import com.poster.requests.ProductCreateRequest;
import com.poster.requests.ProductReviewAggregateListRequest;
import com.poster.requests.ProductReviewCreateRequest;
import com.poster.requests.ProductReviewListRequest;
import com.poster.requests.ProductReviewUpdateRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping({"/api/products"})
public class ProductController {

  @Autowired FunctionalityService functionalityService;

  @PostMapping(value = "/")
  public Product createProduct(@RequestBody(required = true) ProductCreateRequest productCreateRequest) {
    return functionalityService.createProduct(productCreateRequest);
  }

  @PostMapping(value = "/review")
  public ProductReview createProductReview(@RequestBody(required = true) ProductReviewCreateRequest productReviewCreateRequest) {
    productReviewCreateRequest.setUserId(Context.getContext().getUserId());
    return functionalityService.createProductReview(productReviewCreateRequest);
  }

  @PutMapping(value = "/review")
  public ProductReview updateProductReview(@RequestBody(required = true) ProductReviewUpdateRequest productReviewUpdateRequest) {
    productReviewUpdateRequest.setUserId(Context.getContext().getUserId());
    return functionalityService.updateProductReview(productReviewUpdateRequest);
  }

  @PostMapping(value = "/list")
  public List<Product> createProductReview(@RequestBody(required = false) BaseListRequest listRequest) {
    return functionalityService.listReviewBase(listRequest);
  }

  @PostMapping(value = "/review/list")
  public List<ProductReview> listProductReview(@RequestBody(required = false) ProductReviewListRequest listRequest) {
    return functionalityService.listReview(listRequest);
  }

  @PostMapping(value = "/review/aggregate/list")
  public List<ProductReviewAggregate> listProductReviewAggregate(@RequestBody(required = false) ProductReviewAggregateListRequest listRequest) {
    return functionalityService.listReviewAggregate(listRequest);
  }

}
