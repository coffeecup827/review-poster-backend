package com.poster.app.controllers;

import com.poster.app.services.FunctionalityService;
import com.poster.models.User;
import com.poster.requests.UserCreateRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/api/users"})
public class UserController {

  @Autowired FunctionalityService functionalityService;

  @PostMapping(value = "/")
  public User createUser(@RequestBody(required = true) UserCreateRequest userCreateRequest) {
    return functionalityService.createUser(userCreateRequest);
  }

}
