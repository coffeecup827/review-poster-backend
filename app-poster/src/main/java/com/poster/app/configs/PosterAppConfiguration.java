package com.poster.app.configs;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EntityScan(basePackages = {"com.atomify.*","com.poster.*"})
@ComponentScan(basePackages = {"com.atomify.*","com.poster.*"})
public class PosterAppConfiguration implements WebMvcConfigurer {

  @Bean
  public PosterAppAppInterceptor getClusterAppInterceptor() {
    return new PosterAppAppInterceptor();
  }
  @Override
  public void addInterceptors(InterceptorRegistry registry) {

    registry
    .addInterceptor(getClusterAppInterceptor())
    .addPathPatterns("/api/**");

  }
}
