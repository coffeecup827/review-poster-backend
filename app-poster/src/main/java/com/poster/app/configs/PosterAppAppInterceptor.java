package com.poster.app.configs;

import com.atomify.configs.AbstractAppInterceptor;
import com.atomify.configs.Context;
import com.atomify.configs.DefaultContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PosterAppAppInterceptor extends AbstractAppInterceptor {
  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
    return super.preHandle(request, response, handler);
  }
}
