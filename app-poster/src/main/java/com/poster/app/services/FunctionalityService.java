package com.poster.app.services;

import com.atomify.BaseListRequest;
import com.poster.models.Product;
import com.poster.models.ProductReview;
import com.poster.models.ProductReviewAggregate;
import com.poster.models.User;
import com.poster.models.UserProductReviewInfo;
import com.poster.requests.ProductCreateRequest;
import com.poster.requests.ProductReviewAggregateListRequest;
import com.poster.requests.ProductReviewCreateRequest;
import com.poster.requests.ProductReviewInfoListRequest;
import com.poster.requests.ProductReviewListRequest;
import com.poster.requests.ProductReviewUpdateRequest;
import com.poster.requests.UserCreateRequest;
import com.poster.services.ProductService;
import com.poster.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class FunctionalityService {
  @Autowired
  ProductService productService;
  @Autowired
  UserService userService;

  @Transactional
  public Product createProduct(ProductCreateRequest productCreateRequest) {
    return productService.createProduct(productCreateRequest);
  }

  @Transactional
  public User createUser(UserCreateRequest userCreateRequest) {
    return userService.createUser(userCreateRequest);
  }

  @Transactional
  public ProductReview createProductReview(ProductReviewCreateRequest productReviewCreateRequest) {
    return productService.createReview(productReviewCreateRequest);
  }

  public List<Product> listReviewBase(BaseListRequest listRequest) {
    return this.productService.listReviewBase(listRequest);
  }

  public List<ProductReview> listReview(ProductReviewListRequest listRequest) {
    return this.productService.listReview(listRequest);
  }

  public List<ProductReviewAggregate> listReviewAggregate(
      ProductReviewAggregateListRequest listRequest) {
    return this.productService.listReviewAggregate(listRequest);
  }

  public List<UserProductReviewInfo> listReviewInfo(ProductReviewInfoListRequest listRequest) {
    return this.productService.listReviewInfo(listRequest);
  }

  @Transactional
  public ProductReview updateProductReview(ProductReviewUpdateRequest productReviewUpdateRequest) {
    return this.productService.updateReview(productReviewUpdateRequest);
  }
}
