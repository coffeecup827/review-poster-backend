
ALTER TABLE products
  ADD COLUMN latest_high_score_review_id bigint unsigned NULL;

ALTER TABLE products
  ADD COLUMN latest_low_score_review_id bigint unsigned NULL;

ALTER TABLE products
  ADD COLUMN review_score tinyint unsigned DEFAULT '0';

ALTER TABLE products
  ADD COLUMN total_reviews bigint unsigned DEFAULT '0';
