
CREATE TABLE products (
  id                        bigint unsigned NOT NULL AUTO_INCREMENT,
  name                      varchar(255) NOT NULL,
  description               text,
  created_at                bigint unsigned NOT NULL,
  updated_at                bigint unsigned NOT NULL,
  created_by                bigint unsigned NOT NULL,
  updated_by                bigint unsigned NOT NULL,
  row_version               bigint unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (id),
  UNIQUE KEY products_unq_product_name (name)
);

CREATE TABLE product_reviews (
  id                        bigint unsigned NOT NULL AUTO_INCREMENT,
  score                     tinyint unsigned NOT NULL,
  review                    text,
  upvotes                   bigint unsigned NOT NULL,
  downvotes                 bigint unsigned NOT NULL,
  product_id                bigint unsigned NOT NULL,
  user_id                   bigint unsigned NOT NULL,
  created_at                bigint unsigned NOT NULL,
  updated_at                bigint unsigned NOT NULL,
  created_by                bigint unsigned NOT NULL,
  updated_by                bigint unsigned NOT NULL,
  row_version               bigint unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (id),
  KEY product_id_index (product_id),
  KEY user_id_index (user_id),
  UNIQUE KEY product_reviews_unq_user_review_per_product (product_id, user_id)
);

CREATE TABLE product_review_aggregates (
  id                        bigint unsigned NOT NULL AUTO_INCREMENT,
  one_star_count            bigint unsigned NOT NULL,
  two_star_count            bigint unsigned NOT NULL,
  three_star_count          bigint unsigned NOT NULL,
  four_star_count           bigint unsigned NOT NULL,
  five_star_count           bigint unsigned NOT NULL,
  product_id                bigint unsigned NOT NULL,
  created_at                bigint unsigned NOT NULL,
  updated_at                bigint unsigned NOT NULL,
  row_version               bigint unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (id),
  KEY product_id_index (product_id),
  UNIQUE KEY product_review_aggregates_unq_product_aggregate (product_id)
);

CREATE TABLE users (
  id                        bigint unsigned NOT NULL AUTO_INCREMENT,
  name                      varchar(255) NOT NULL,
  created_at                bigint unsigned NOT NULL,
  updated_at                bigint unsigned NOT NULL,
  created_by                bigint unsigned NOT NULL,
  updated_by                bigint unsigned NOT NULL,
  row_version               bigint unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (id),
  UNIQUE KEY users_unq_user_name (name)
);


CREATE TABLE user_product_review_info (
  id                        bigint unsigned NOT NULL AUTO_INCREMENT,
  vote                      tinyint NOT NULL,
  product_review_id         bigint unsigned NOT NULL,
  product_id                bigint unsigned NOT NULL,
  user_id                   bigint unsigned NOT NULL,
  created_at                bigint unsigned NOT NULL,
  updated_at                bigint unsigned NOT NULL,
  created_by                bigint unsigned NOT NULL,
  updated_by                bigint unsigned NOT NULL,
  row_version               bigint unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (id),
  KEY product_id_index (product_id),
  KEY user_id_index (user_id),
  UNIQUE KEY user_product_review_info_unq_user_review_info (product_review_id, user_id)
);
