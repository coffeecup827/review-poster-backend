package com.atomify.requests;

public abstract class AbstractReviewUpdateRequest extends BaseRequest {
  private long reviewId;
  private int score;
  private String review;

  public long getReviewId() {
    return reviewId;
  }

  public void setReviewId(long reviewId) {
    this.reviewId = reviewId;
  }

  public int getScore() {
    return score;
  }

  public void setScore(int score) {
    this.score = score;
  }

  public String getReview() {
    return review;
  }

  public void setReview(String review) {
    this.review = review;
  }

  public abstract void setReviewById(long id);
  public abstract long getReviewById();
}
