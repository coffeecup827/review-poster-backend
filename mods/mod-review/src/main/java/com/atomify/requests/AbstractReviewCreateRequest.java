package com.atomify.requests;

public abstract class AbstractReviewCreateRequest extends BaseRequest {
  private int score;
  private String review;

  public int getScore() {
    return score;
  }

  public void setScore(int score) {
    this.score = score;
  }

  public String getReview() {
    return review;
  }

  public void setReview(String review) {
    this.review = review;
  }

  public abstract long getReviewOnId();

  public abstract long getReviewById();

  public abstract void setReviewOnId(long id);

  public abstract void setReviewById(long id);
}
