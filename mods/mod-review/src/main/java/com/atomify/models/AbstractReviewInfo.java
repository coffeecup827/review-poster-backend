package com.atomify.models;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class AbstractReviewInfo extends ChangeTrackedModel {

  @Column
  private long reviewId;

  @Column
  private int score;

  public long getReviewId() {
    return reviewId;
  }

  public void setReviewId(long reviewId) {
    this.reviewId = reviewId;
  }

  public int getScore() {
    return score;
  }

  public void setScore(int score) {
    this.score = score;
  }

  public abstract long getReviewOnId();

  public abstract long getReviewById();

  public abstract void setReviewOnId(long id);

  public abstract void setReviewById(long id);
}
