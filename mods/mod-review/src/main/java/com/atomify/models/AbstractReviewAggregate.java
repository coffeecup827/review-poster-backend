package com.atomify.models;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class AbstractReviewAggregate extends AbstractModel {
  @Column
  private long oneStarCount;

  @Column
  private long twoStarCount;

  @Column
  private long threeStarCount;

  @Column
  private long fourStarCount;

  @Column
  private long fiveStarCount;

  public long getOneStarCount() {
    return oneStarCount;
  }

  public void setOneStarCount(long oneStarCount) {
    this.oneStarCount = oneStarCount;
  }

  public long getTwoStarCount() {
    return twoStarCount;
  }

  public void setTwoStarCount(long twoStarCount) {
    this.twoStarCount = twoStarCount;
  }

  public long getThreeStarCount() {
    return threeStarCount;
  }

  public void setThreeStarCount(long threeStarCount) {
    this.threeStarCount = threeStarCount;
  }

  public long getFourStarCount() {
    return fourStarCount;
  }

  public void setFourStarCount(long fourStarCount) {
    this.fourStarCount = fourStarCount;
  }

  public long getFiveStarCount() {
    return fiveStarCount;
  }

  public void setFiveStarCount(long fiveStarCount) {
    this.fiveStarCount = fiveStarCount;
  }

  public abstract long getReviewOnId();

  public abstract void setReviewOnId(long id);

}
