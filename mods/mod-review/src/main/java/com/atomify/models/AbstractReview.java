package com.atomify.models;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class AbstractReview extends ChangeTrackedModel {
  @Column
  private int score;

  @Column
  private String review;

  @Column
  private long upvotes;

  @Column
  private long downvotes;

  public int getScore() {
    return score;
  }

  public void setScore(int score) {
    this.score = score;
  }

  public String getReview() {
    return review;
  }

  public void setReview(String review) {
    this.review = review;
  }

  public long getUpvotes() {
    return upvotes;
  }

  public void setUpvotes(long upvotes) {
    this.upvotes = upvotes;
  }

  public long getDownvotes() {
    return downvotes;
  }

  public void setDownvotes(long downvotes) {
    this.downvotes = downvotes;
  }

  public abstract long getReviewOnId();

  public abstract long getReviewById();

  public abstract void setReviewOnId(long id);

  public abstract void setReviewById(long id);
}
