package com.atomify.models;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class AbstractReviewBase extends ChangeTrackedModel {
  @Column
  private long totalReviews;

  @Column
  private int reviewScore;

  @Column
  private float averageReview;

  public long getTotalReviews() {
    return totalReviews;
  }

  public void setTotalReviews(long totalReviews) {
    this.totalReviews = totalReviews;
  }

  public int getReviewScore() {
    return reviewScore;
  }

  public void setReviewScore(int reviewScore) {
    this.reviewScore = reviewScore;
  }

  public float getAverageReview() {
    return averageReview;
  }

  public void setAverageReview(float averageReview) {
    this.averageReview = averageReview;
  }
}
