package com.atomify.services;

import com.atomify.BaseListRequest;
import com.atomify.DAOs.AbstractModelDAO;
import com.atomify.models.AbstractReview;
import com.atomify.models.AbstractReviewAggregate;
import com.atomify.models.AbstractReviewBase;
import com.atomify.models.AbstractReviewInfo;
import com.atomify.requests.AbstractReviewCreateRequest;
import com.atomify.requests.AbstractReviewUpdateRequest;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public abstract class AbstractReviewService<ReviewBase extends AbstractReviewBase, ReviewAggregate extends AbstractReviewAggregate,
    Review extends AbstractReview, ReviewInfo extends AbstractReviewInfo> {

  public abstract AbstractModelDAO<ReviewBase> getReviewBaseDAO();
  public abstract AbstractModelDAO<ReviewAggregate> getReviewAggregateDAO();
  public abstract AbstractModelDAO<Review> getReviewDAO();
  public abstract AbstractModelDAO<ReviewInfo> getReviewInfoDAO();

  public abstract ReviewBase getReviewBase(long reviewOnId);
  public abstract ReviewAggregate getReviewAggregate(long reviewOnId);
  public abstract ReviewInfo getReviewInfo(long reviewOnId, long reviewById, long reviewId);
  public abstract Review getReview(long reviewId);

  public <U extends BaseListRequest> List<ReviewBase> listReviewBase(U listRequest) {
    return this.getReviewBaseDAO().find(listRequest);
  }
  public <U extends BaseListRequest> List<Review> listReview(U listRequest) {
    return this.getReviewDAO().find(listRequest);
  }
  public <U extends BaseListRequest> List<ReviewAggregate> listReviewAggregate(U listRequest) {
    return this.getReviewAggregateDAO().find(listRequest);
  }
  public <U extends BaseListRequest> List<ReviewInfo> listReviewInfo(U listRequest) {
    return this.getReviewInfoDAO().find(listRequest);
  }

  public ReviewAggregate createAggregate(long reviewOnId) {
    ReviewAggregate aggregate = getReviewAggregateDAO().getNewInstance();
    aggregate.setReviewOnId(reviewOnId);
    getReviewAggregateDAO().create(aggregate);
    return aggregate;
  }

  public Review createReview(AbstractReviewCreateRequest abstractReviewCreateRequest) {
    Review review = getReviewDAO().getNewInstance();
    review.setReview(abstractReviewCreateRequest.getReview());
    review.setReviewById(abstractReviewCreateRequest.getReviewById());
    review.setReviewOnId(abstractReviewCreateRequest.getReviewOnId());
    review.setScore(abstractReviewCreateRequest.getScore());

    getReviewDAO().create(review);

    ReviewAggregate aggregate = getReviewAggregate(abstractReviewCreateRequest.getReviewOnId());

    incrementStarCount(aggregate, abstractReviewCreateRequest.getScore());

    ReviewBase reviewBase = getReviewBase(abstractReviewCreateRequest.getReviewOnId());
    reviewBase.setTotalReviews(reviewBase.getTotalReviews() + 1);
    reviewBase.setReviewScore(reviewBase.getReviewScore() + abstractReviewCreateRequest.getScore());

    reviewBase.setAverageReview((float)reviewBase.getReviewScore()/reviewBase.getTotalReviews());

    return review;
  }

  public Review updateReview(AbstractReviewUpdateRequest updateRequest) {
    Review review = getReview(updateRequest.getReviewId());
    ReviewAggregate aggregate = getReviewAggregate(review.getReviewOnId());
    ReviewBase reviewBase = getReviewBase(review.getReviewOnId());

    decrementStarCount(aggregate, review.getScore());
    reviewBase.setReviewScore(reviewBase.getReviewScore() - review.getScore());

    review.setScore(updateRequest.getScore());
    review.setReview(updateRequest.getReview());

    incrementStarCount(aggregate, review.getScore());
    reviewBase.setReviewScore(reviewBase.getReviewScore() + review.getScore());

    reviewBase.setAverageReview((float)reviewBase.getReviewScore()/reviewBase.getTotalReviews());

    return  review;
  }

  void incrementStarCount(ReviewAggregate aggregate,int score) {
    switch (score) {
    case 1:
      aggregate.setOneStarCount(aggregate.getOneStarCount() + 1);
      break;
    case 2:
      aggregate.setTwoStarCount(aggregate.getTwoStarCount() + 1);
      break;
    case 3:
      aggregate.setThreeStarCount(aggregate.getThreeStarCount() + 1);
      break;
    case 4:
      aggregate.setFourStarCount(aggregate.getFourStarCount() + 1);
      break;
    case 5:
      aggregate.setFiveStarCount(aggregate.getFiveStarCount() + 1);
      break;
    }
  }

  void decrementStarCount(ReviewAggregate aggregate, int score) {
    switch (score) {
    case 1:
      aggregate.setOneStarCount(aggregate.getOneStarCount() - 1);
      break;
    case 2:
      aggregate.setTwoStarCount(aggregate.getTwoStarCount() - 1);
      break;
    case 3:
      aggregate.setThreeStarCount(aggregate.getThreeStarCount() - 1);
      break;
    case 4:
      aggregate.setFourStarCount(aggregate.getFourStarCount() - 1);
      break;
    case 5:
      aggregate.setFiveStarCount(aggregate.getFiveStarCount() - 1);
      break;
    }
  }
}
