package com.poster.services;

import com.atomify.BaseListRequest;
import com.atomify.DAOs.AbstractModelDAO;
import com.atomify.services.AbstractReviewService;
import com.poster.dao.ProductDAO;
import com.poster.dao.ProductReviewAggregateDAO;
import com.poster.dao.ProductReviewDAO;
import com.poster.dao.UserProductReviewInfoDAO;
import com.poster.models.Product;
import com.poster.models.ProductReview;
import com.poster.models.ProductReviewAggregate;
import com.poster.models.UserProductReviewInfo;
import com.poster.requests.ProductCreateRequest;
import com.poster.requests.ProductReviewAggregateListRequest;
import com.poster.requests.ProductReviewInfoListRequest;
import com.poster.requests.ProductReviewListRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
public class ProductService extends AbstractReviewService<Product, ProductReviewAggregate, ProductReview, UserProductReviewInfo> {
  @Autowired ProductDAO productDAO;
  @Autowired ProductReviewDAO productReviewDAO;
  @Autowired ProductReviewAggregateDAO productReviewAggregateDAO;
  @Autowired UserProductReviewInfoDAO userProductReviewInfoDAO;

  @Override public AbstractModelDAO<Product> getReviewBaseDAO() {
    return this.productDAO;
  }

  @Override public AbstractModelDAO<ProductReviewAggregate> getReviewAggregateDAO() {
    return this.productReviewAggregateDAO;
  }

  @Override public AbstractModelDAO<ProductReview> getReviewDAO() {
    return this.productReviewDAO;
  }

  @Override public AbstractModelDAO<UserProductReviewInfo> getReviewInfoDAO() {
    return this.userProductReviewInfoDAO;
  }

  @Override public Product getReviewBase(long reviewOnId) {
    BaseListRequest listRequest = new BaseListRequest();
    listRequest.setFilterByIds(Collections.singleton(reviewOnId));
    return productDAO.findOne(listRequest);
  }

  @Override public ProductReviewAggregate getReviewAggregate(long reviewOnId) {
    ProductReviewAggregateListRequest listRequest = new ProductReviewAggregateListRequest();
    listRequest.setFilterByProductIds(Collections.singleton(reviewOnId));
    return productReviewAggregateDAO.findOne(listRequest);
  }

  @Override public UserProductReviewInfo getReviewInfo(long reviewOnId, long reviewById, long reviewId) {
    ProductReviewInfoListRequest listRequest = new ProductReviewInfoListRequest();
    listRequest.setFilterByProductIds(Collections.singleton(reviewOnId));
    listRequest.setFilterByUserIds(Collections.singleton(reviewById));
    listRequest.setFilterByReviewIds(Collections.singleton(reviewId));
    return userProductReviewInfoDAO.findOne(listRequest);
  }

  @Override public ProductReview getReview(long reviewId) {
    ProductReviewListRequest listRequest = new ProductReviewListRequest();
    listRequest.setFilterByIds(Collections.singleton(reviewId));
    return productReviewDAO.findOne(listRequest);
  }

  public Product createProduct(ProductCreateRequest productCreateRequest) {
    Product product = new Product();
    product.setName(productCreateRequest.getName());
    product.setDescription(productCreateRequest.getDescription());

    product = productDAO.create(product);

    createAggregate(product.getId());
    return product;
  }
}
