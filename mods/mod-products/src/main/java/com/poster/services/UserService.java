package com.poster.services;

import com.poster.dao.UserDAO;
import com.poster.models.User;
import com.poster.requests.UserCreateRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserService {
  @Autowired UserDAO userDAO;

  public User createUser(UserCreateRequest userCreateRequest) {
    User user = new User();
    user.setName(userCreateRequest.getName());
    return userDAO.create(user);
  }
}
