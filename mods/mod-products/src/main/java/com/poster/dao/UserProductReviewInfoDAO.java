package com.poster.dao;

import com.atomify.BaseListRequest;
import com.atomify.DAOs.ChangeTrackedModelDAO;
import com.poster.models.UserProductReviewInfo;
import com.poster.requests.ProductReviewInfoListRequest;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Repository
public class UserProductReviewInfoDAO extends ChangeTrackedModelDAO<UserProductReviewInfo> {
  @Override public <U extends BaseListRequest> CriteriaQuery<UserProductReviewInfo> buildQuery(
      CriteriaQuery<UserProductReviewInfo> criteriaQuery, CriteriaBuilder criteriaBuilder,
      Root<UserProductReviewInfo> root, U listRequest) {
    ProductReviewInfoListRequest infoListRequest = (ProductReviewInfoListRequest) listRequest;
    if (infoListRequest.getFilterByProductIds() != null && !infoListRequest.getFilterByProductIds().isEmpty()) {
      criteriaQuery.where(
          criteriaBuilder.in(root.get("productId")).value(infoListRequest.getFilterByProductIds())
      );
    }
    if (infoListRequest.getFilterByUserIds() != null && !infoListRequest.getFilterByUserIds().isEmpty()) {
      criteriaQuery.where(
          criteriaBuilder.in(root.get("userId")).value(infoListRequest.getFilterByProductIds())
      );
    }
    if (infoListRequest.getFilterByReviewIds() != null && !infoListRequest.getFilterByReviewIds().isEmpty()) {
      criteriaQuery.where(
          criteriaBuilder.in(root.get("reviewId")).value(infoListRequest.getFilterByProductIds())
      );
    }
    return criteriaQuery;
  }
}
