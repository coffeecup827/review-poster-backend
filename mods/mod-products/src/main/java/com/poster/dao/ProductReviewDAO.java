package com.poster.dao;

import com.atomify.BaseListRequest;
import com.atomify.DAOs.ChangeTrackedModelDAO;
import com.poster.models.ProductReview;
import com.poster.requests.ProductReviewListRequest;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Repository
public class ProductReviewDAO extends ChangeTrackedModelDAO<ProductReview> {
  @Override public <U extends BaseListRequest> CriteriaQuery<ProductReview> buildQuery(
      CriteriaQuery<ProductReview> criteriaQuery, CriteriaBuilder criteriaBuilder,
      Root<ProductReview> root, U listRequest) {
    ProductReviewListRequest reviewListRequest = (ProductReviewListRequest) listRequest;
    if (reviewListRequest.getFilterByProductIds() != null && !reviewListRequest.getFilterByProductIds().isEmpty()) {
      criteriaQuery.where(
          criteriaBuilder.in(root.get("productId")).value(reviewListRequest.getFilterByProductIds())
      );
    }
    if (reviewListRequest.getFilterByUserIds() != null && !reviewListRequest.getFilterByUserIds().isEmpty()) {
      criteriaQuery.where(
          criteriaBuilder.in(root.get("userId")).value(reviewListRequest.getFilterByProductIds())
      );
    }
    if (reviewListRequest.getFilterByScore() != null && !reviewListRequest.getFilterByScore().isEmpty()) {
      criteriaQuery.where(
          criteriaBuilder.in(root.get("score")).value(reviewListRequest.getFilterByProductIds())
      );
    }
    return criteriaQuery;
  }
}
