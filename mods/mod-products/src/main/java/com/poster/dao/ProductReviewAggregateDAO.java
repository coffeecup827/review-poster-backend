package com.poster.dao;

import com.atomify.BaseListRequest;
import com.atomify.DAOs.AbstractModelDAO;
import com.poster.models.ProductReviewAggregate;
import com.poster.requests.ProductReviewAggregateListRequest;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Repository
public class ProductReviewAggregateDAO extends AbstractModelDAO<ProductReviewAggregate> {
  @Override public <U extends BaseListRequest> CriteriaQuery<ProductReviewAggregate> buildQuery(
      CriteriaQuery<ProductReviewAggregate> criteriaQuery, CriteriaBuilder criteriaBuilder,
      Root<ProductReviewAggregate> root, U listRequest) {
    ProductReviewAggregateListRequest productReviewAggregateListRequest = (ProductReviewAggregateListRequest) listRequest;
    if (productReviewAggregateListRequest.getFilterByProductIds() != null && !productReviewAggregateListRequest.getFilterByProductIds().isEmpty()) {
      criteriaQuery.where(
        criteriaBuilder.in(root.get("productId")).value(productReviewAggregateListRequest.getFilterByProductIds())
      );
    }
    return criteriaQuery;
  }
}
