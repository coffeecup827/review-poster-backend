package com.poster.dao;

import com.atomify.DAOs.ChangeTrackedModelDAO;
import com.poster.models.Product;
import org.springframework.stereotype.Repository;

@Repository
public class ProductDAO extends ChangeTrackedModelDAO<Product> {
}
