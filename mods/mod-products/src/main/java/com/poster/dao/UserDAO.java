package com.poster.dao;

import com.atomify.DAOs.ChangeTrackedModelDAO;
import com.poster.models.User;
import org.springframework.stereotype.Repository;

@Repository
public class UserDAO extends ChangeTrackedModelDAO<User> {
}
