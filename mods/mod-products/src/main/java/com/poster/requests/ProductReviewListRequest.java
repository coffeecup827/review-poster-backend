package com.poster.requests;

import com.atomify.BaseListRequest;

import java.util.Set;

public class ProductReviewListRequest extends BaseListRequest {
  private Set<Long> filterByProductIds;
  private Set<Long> filterByUserIds;
  private Set<Long> filterByScore;

  public Set<Long> getFilterByProductIds() {
    return filterByProductIds;
  }

  public void setFilterByProductIds(Set<Long> filterByProductIds) {
    this.filterByProductIds = filterByProductIds;
  }

  public Set<Long> getFilterByUserIds() {
    return filterByUserIds;
  }

  public void setFilterByUserIds(Set<Long> filterByUserIds) {
    this.filterByUserIds = filterByUserIds;
  }

  public Set<Long> getFilterByScore() {
    return filterByScore;
  }

  public void setFilterByScore(Set<Long> filterByScore) {
    this.filterByScore = filterByScore;
  }
}
