package com.poster.requests;

import com.atomify.BaseListRequest;

import java.util.Set;

public class ProductReviewInfoListRequest extends BaseListRequest {

  private Set<Long> filterByProductIds;
  private Set<Long> filterByUserIds;
  private Set<Long> filterByReviewIds;

  public Set<Long> getFilterByUserIds() {
    return filterByUserIds;
  }

  public void setFilterByUserIds(Set<Long> filterByUserIds) {
    this.filterByUserIds = filterByUserIds;
  }

  public Set<Long> getFilterByReviewIds() {
    return filterByReviewIds;
  }

  public void setFilterByReviewIds(Set<Long> filterByReviewIds) {
    this.filterByReviewIds = filterByReviewIds;
  }

  public Set<Long> getFilterByProductIds() {
    return filterByProductIds;
  }

  public void setFilterByProductIds(Set<Long> filterByProductIds) {
    this.filterByProductIds = filterByProductIds;
  }


}
