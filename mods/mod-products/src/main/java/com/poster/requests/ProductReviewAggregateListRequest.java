package com.poster.requests;

import com.atomify.BaseListRequest;

import java.util.Set;

public class ProductReviewAggregateListRequest extends BaseListRequest {

  private Set<Long> filterByProductIds;

  public Set<Long> getFilterByProductIds() {
    return filterByProductIds;
  }

  public void setFilterByProductIds(Set<Long> filterByProductIds) {
    this.filterByProductIds = filterByProductIds;
  }
}
