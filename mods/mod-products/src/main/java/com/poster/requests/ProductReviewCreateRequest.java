package com.poster.requests;

import com.atomify.requests.AbstractReviewCreateRequest;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class ProductReviewCreateRequest extends AbstractReviewCreateRequest {

  private long productId;

  @JsonIgnore
  private long userId;

  public long getProductId() {
    return productId;
  }

  public void setProductId(long productId) {
    this.productId = productId;
  }

  public long getUserId() {
    return userId;
  }

  @JsonIgnore
  public void setUserId(long userId) {
    this.userId = userId;
  }

  @Override public long getReviewOnId() {
    return this.getProductId();
  }

  @Override public long getReviewById() {
    return this.getUserId();
  }

  @Override public void setReviewOnId(long id) {
    this.setProductId(id);
  }

  @Override public void setReviewById(long id) {
    this.setUserId(id);
  }
}
