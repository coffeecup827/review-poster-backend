package com.poster.requests;

import com.atomify.requests.AbstractReviewUpdateRequest;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class ProductReviewUpdateRequest extends AbstractReviewUpdateRequest {

  @JsonIgnore
  private long userId;

  public long getUserId() {
    return userId;
  }

  @JsonIgnore
  public void setUserId(long userId) {
    this.userId = userId;
  }

  @Override public long getReviewById() {
    return this.getUserId();
  }

  @Override public void setReviewById(long id) {
    this.setUserId(id);
  }

}
