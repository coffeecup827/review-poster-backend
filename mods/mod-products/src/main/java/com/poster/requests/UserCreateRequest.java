package com.poster.requests;

import com.atomify.requests.BaseRequest;

public class UserCreateRequest extends BaseRequest {
  private String name;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
