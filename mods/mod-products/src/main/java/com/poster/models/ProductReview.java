package com.poster.models;

import com.atomify.models.AbstractReview;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "product_reviews")
public class ProductReview extends AbstractReview {

  @Column
  private long productId;

  @Column
  private long userId;

  public long getProductId() {
    return productId;
  }

  public void setProductId(long productId) {
    this.productId = productId;
  }

  public long getUserId() {
    return userId;
  }

  public void setUserId(long userId) {
    this.userId = userId;
  }

  @Override public long getReviewOnId() {
    return this.getProductId();
  }

  @Override public long getReviewById() {
    return this.getUserId();
  }

  @Override public void setReviewOnId(long id) {
    this.setProductId(id);
  }

  @Override public void setReviewById(long id) {
    this.setUserId(id);
  }
}
