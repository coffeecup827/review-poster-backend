package com.poster.models;

import com.atomify.models.AbstractReviewAggregate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "product_review_aggregates")
public class ProductReviewAggregate extends AbstractReviewAggregate {

  @Column
  private long productId;

  public long getProductId() {
    return productId;
  }

  public void setProductId(long productId) {
    this.productId = productId;
  }

  @Override public long getReviewOnId() {
    return this.getProductId();
  }

  @Override public void setReviewOnId(long id) {
    this.setProductId(id);
  }

}
